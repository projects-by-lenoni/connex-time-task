const getTime = async () => {
    let url = 'http://localhost:3001/time';
    let data = await (await fetch(url, {
        headers: new Headers({
            'Authorization': 'mysecrettoken',
            'Content-Type': 'application/json',
        })
    })).json();
    return data.epoch;
}

const getMetrics = async () => {
    let url = 'http://localhost:3001/metrics';
    let data = await (await fetch(url, {
        headers: new Headers({
            'Authorization': 'mysecrettoken',
        })
    })).text();
    return data;
}

export default { getTime, getMetrics }