import { Loader } from "./loader";

// TIME SIDE OF APP
interface TimeViewInterface {
    serverTime: number;
    difference: number;
}

export const TimeView:React.FC<TimeViewInterface> = ({ serverTime, difference }) => {
    if (!Number(serverTime) || !difference) {
        return (
          <div className='app-side time-view'>
            <div className='title'>Time</div>
            <Loader />
          </div>
        )
      }
      
    return (
      <div className='app-side time-view'>
        <div className='title'>Time</div>
        <div className='time'>{serverTime}</div>
        <div className='time-diff'>{new Intl.DateTimeFormat('en-GB', {hour: "numeric", minute: "numeric", second: "numeric"}).format(difference)}</div>
      </div>
    );
  }