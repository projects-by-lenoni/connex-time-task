var app = require('./app');
var request = require('supertest');

describe('getTime', () => {
    it('Should return time value in a JSON object', (done) => {
        request(app)
        .get('/time')
        .set('Authorization', 'mysecrettoken')
        .expect(200, done)
    });

    it('Should reject unauthorized users', (done) => {
        request(app)
        .get('/time')
        .expect(403, done)
    });
});

describe('getMetrics', () => {
    it('Should return metrics in a text format', (done) => {
        request(app)
        .get('/metrics')
        .set('Authorization', 'mysecrettoken')
        .expect(200, done)
    });

    it('Should reject unauthorized users', (done) => {
        request(app)
        .get('/metrics')
        .expect(403, done)
    });
});