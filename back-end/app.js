var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cors = require("cors");
var client = require('prom-client');
var jsonValidator = require('jsonschema').Validator;

var indexRouter = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Create a Registry which registers the metrics
var register = new client.Registry();
// Enable the collection of default metrics
client.collectDefaultMetrics({ register });

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

// ENDPOINTS

// Index Endpoint
app.use('/', indexRouter);

// Time Endpoint
var timeSchema = {
  'properties': {
    'epoch': {
      'description': 'The current server time, in epoch seconds, at time of processing the request.',
      'type': 'number'
    }
  },
  'required': ['epoch'],
  'type': 'object'
}
var validator = new jsonValidator();
validator.addSchema(timeSchema,'/time');

app.get('/time', function (req, res) {
  // Return 403 error header does not include ‘mysecrettoken’
  if (req.get('Authorization') !== 'mysecrettoken') { 
    res.status(403).send("Access denied."); 
  } else {
    try { 
      let time = { epoch: Date.now() };
      // Return time if it is valid agaisnt the schema
      if (validator.validate(time, timeSchema, 'throwError').valid) {
        res.json(time);
      } else {
        res.status(500).send("Time data is not valid."); 
      }
    } catch(error) { 
        res.status(403).send("Something went wrong.");  
    }
  }
});

// Metrics Endpoint
app.get('/metrics', async function (req, res) {
  if (req.get('Authorization') !== 'mysecrettoken') { 
    res.status(403).send("Access denied."); 
  } else {
    // Return all metrics in Prometheus exposition format
    res.set('Content-Type', register.contentType);
    let metrics = await register.metrics();
    res.send(metrics);
  }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
