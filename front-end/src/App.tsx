import './App.css';

import React, { useEffect, useState } from 'react';
import Services from './services/services';
import { MetricsView } from './components/metrics-view';
import { TimeView } from './components/time-view';

function App() {
  const [serverTime, setServerTime] = useState(0);
  const [difference, setDifference] = useState(0);
  const [metrics, setMetrics] = useState('');

  // Get Time
  useEffect(() => {
    Services.getTime().then(time => {
      setServerTime(time)
    });
    function getTime() {
      Services.getTime().then(time => {
        setServerTime(time)
      });
    }

    getTime()
    // Time interval for server time
    const interval = setInterval(() => {
      setServerTime(NaN);
      getTime();
    }, 30000);

    return () => clearInterval(interval);
  }, []);


  // Initialise Time Difference
  useEffect(() => {    
    // Time interval for time difference
    const interval = setInterval(() => {
      // Correct time difference affected by timezone offset
      setDifference(Date.now() - serverTime + (new Date(serverTime).getTimezoneOffset() * 60000));
    }, 1000);

    return () => clearInterval(interval);
  }, [serverTime]);

  
  // Get Metrics 
  useEffect(() => {
    Services.getMetrics().then(metrics => {
      setMetrics(metrics);
    });
    function getMetrics() {
      Services.getMetrics().then(metrics => {
        setMetrics(metrics);
      });
    }

    getMetrics();
    // Time interval for metrics
    const interval = setInterval(() => {
      setMetrics('');
      getMetrics();
    }, 30000);

    return () => clearInterval(interval);    
  }, []);

  return (
    <div className='main'>
      <TimeView serverTime={serverTime} difference={difference} />
      <MetricsView metrics={metrics} />
    </div>
  );
}

export default App;
