# Connex One - Full Stack Developer Task

## Installation

```bash
git clone https://gitlab.com/projects-by-lenoni/connex-one-task.git
```

To Run Server

```bash
cd back-end
npm install
npm start
```


To Run Client
```bash
cd front-end
npm install
npm start
```

The client runs on port 3000 and the server runs on port 3001.

#### Note
Client UI has been split vertically rather than 'horizontally' as this is assumed to be a typo.
