import { Loader } from "./loader";

// METRICS SIDE OF APP
interface MetricsViewInterface {
    metrics: string;
}

export const MetricsView:React.FC<MetricsViewInterface> = ({ metrics }) => {
  if (!metrics.length) {
    return (
      <div className='app-side metrics-view'>
        <div className='title'>Metrics</div>
        <Loader />
      </div>
    )
  }

    return (
        <div className='app-side metrics-view'>
          <div className='title'>Metrics</div>
          <pre className='metrics'>{metrics}</pre>
        </div>
      );
  }